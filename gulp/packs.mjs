/* eslint-disable foundry-cpr/logger-after-function-definition */
import fs from "fs-extra";
import log from "fancy-log";
import path from "path";
import { compilePack, extractPack } from "@foundryvtt/foundryvtt-cli";

import PackUtils from "./utils/packUtils.mjs";
import { DEBUG, DEST_DIR, SRC_DIR, MODULE_FILE, PACKS_DIR } from "./config.mjs";

/*
 * Asynchronously extracts packs from the module to YAML fragments.
 *
 * @returns {Promise} A promise which resolves when all packs have been processed.
 * @throws Will throw an error if the path for a specific pack does not exist.
 *
 */
async function extPacks() {
  const fragmentDir = path.resolve(SRC_DIR, PACKS_DIR);
  const sysFile = JSON.parse(
    fs.readFileSync(path.resolve(SRC_DIR, MODULE_FILE))
  );
  let { packs } = sysFile;

  // Because we want to handle deleted items in git properly we first delete
  // the fragmentDir then re-create it before writing out files.
  if (fs.existsSync(fragmentDir)) {
    fs.rmSync(fragmentDir, { recursive: true });
  }
  fs.mkdirSync(fragmentDir, { recursive: true });

  // Loop over each pack in module.json
  const promises = packs.map(async (pack) => {
    const packName = pack.name;
    const packPath = path.resolve(DEST_DIR, pack.path);
    const outputDir = path.resolve(SRC_DIR, pack.path);

    if (DEBUG) {
      log(`DEBUG: Processing ${packName}`);
      log(`DEBUG: packPath ${packPath}`);
      log(`DEBUG: outputDir ${outputDir}`);
    }

    // As we are extracting packs here the packPath should always exist.
    // If the packPath doesn't exist throw and error rather than failing
    // silently.
    if (!fs.pathExistsSync(packPath)) {
      throw new Error(`${packPath} does not exist`);
    }

    // Make sure the outputDir exits
    // We create the packs directory above and create the per pack dirs here
    if (!fs.pathExistsSync(outputDir)) {
      log(`DEBUG: Creating ${outputDir}`);
      fs.mkdirSync(outputDir, { recursive: true });
    }

    // Unpack the database then return the promise so we're async
    // this uses the extractPack function from the foundry-cli
    log(`DEBUG: Extracting ${packPath}`);
    await extractPack(packPath, outputDir, {
      yaml: true,
      yamlOptions: { indent: 2 },
    });
    return [];
  });

  return Promise.all(promises);
}

/**
 * Generate packs by processing YAML fragments.
 *
 * @async
 * @function genPacks
 * @returns {Promise<void[]>} - A Promise that resolves with an array of empty
 *                              arrays once all packs are generated.
 *
 */
async function genPacks() {
  log("Generating Packs...");
  const packsDir = path.resolve(DEST_DIR, PACKS_DIR);
  const sysFile = JSON.parse(
    fs.readFileSync(path.resolve(SRC_DIR, MODULE_FILE))
  );
  const { packs } = sysFile;

  // Because we build the leveldb packs from YAML fragments we don't need the
  // contents of the currently built packs
  if (fs.existsSync(packsDir)) {
    fs.rmSync(packsDir, { recursive: true });
  }
  fs.mkdirSync(packsDir, { recursive: true });

  // Loop over each pack in module.json
  const promises = packs.map(async (pack) => {
    const packName = pack.name;
    const fragmentPath = path.resolve(SRC_DIR, pack.path);
    const outputDir = path.resolve(DEST_DIR, pack.path);

    if (DEBUG) {
      log(`DEBUG: Processing ${packName}`);
    }

    // Make sure the outputDir exits
    // We create the packs directory above and create the per pack dirs here
    if (!fs.pathExistsSync(outputDir)) {
      fs.mkdirSync(outputDir, { recursive: true });
    }

    // Create the fragment dir if it's a new pack and the dir doesn't exist
    if (!fs.pathExistsSync(fragmentPath)) {
      fs.mkdirSync(fragmentPath, { recursive: true });
    }

    // Pack the database then return the promise so we're async
    await compilePack(fragmentPath, outputDir, {
      yaml: true,
      yamlOptions: { indent: 2, sortKeys: true },
    });
    return [];
  });

  return Promise.all(promises);
}

export { extPacks, genPacks };
