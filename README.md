# Cyberpunk RED - Ghost Extraction

<!-- markdownlint-disable-next-line MD033 -->
<div align="center">
![Ghost Extraction](src/images/title.webp)
</div>

Ghost Extraction is a border hopping adventure to Canada and back to find and extract a Militech operative who is M.I.A.

## Disclaimer

This project is under heavy development and should be considered in Alpha. This can mean anything from minor incompatibilities to major changes may be made at any time.

---

> This adventure is unofficial content provided under the [Homebrew Content Policy](https://rtalsoriangames.com/homebrew-content-policy/) of R. Talsorian Games and is not approved or endorsed by RTG. This content references materials that are the property of R. Talsorian Games and its licensees.
