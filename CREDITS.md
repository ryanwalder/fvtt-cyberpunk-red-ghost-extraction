# Credits

## Map Assets

I've not kept proper track of which assets have been used in the creation of this adventure but these are links to the packs/authors I've had active during development.

They are all awesome so check them out.

- [Crosshead](https://www.patreon.com/Crosshead)
- [The Planet Hoppers](https://www.patreon.com/theplanethoppers)
- [DungeonQuill](https://www.patreon.com/dungeonquill)
- [Forgotten Adventues](https://www.forgotten-adventures.net/)
- [Gnome Factory](https://www.patreon.com/gnomefactory)
- [Miska's Maps](https://www.miskasmaps.com)
- [PeaPu](https://www.patreon.com/PeaPu)
- [Tyger_purr](https://cartographyassets.com/creator/tyger_purr/)
- [White Fox Works](https://www.patreon.com/WhiteFoxWorks)
- [Antediluvian](https://cartographyassets.com/assets/5452/antediluvian-architecture/) Architecture by Stovetop
- [AoA_ShadowPattern](https://cartographyassets.com/assets/5499/shadow-pattern/) by AoA
- [Construction](https://cartographyassets.com/assets/9320/construction-chaos/) Chaos by Valstoys
- [Krager's Shadow Pack](https://cartographyassets.com/assets/7713/kragers-shadow-light-pack/) by Krager
- [Modern Plumbing](https://cartographyassets.com/assets/5542/modern-plumbing/) by SairtheBear
- [Spiffeigh's Modern Day Assets](https://cartographyassets.com/assets/spiffeighs-modern-day-assets/) by Spiffeigh

AV Landing Pad inspired by [Gimmie Pig](https://www.patreon.com/GimmiePig/posts)

## Images / Icons / Graphics

- Blueprint Oval Marks by juicy_fish on [Freepik](https://www.freepik.com/free-vector/oval-brushes-1_35202569.htm)
- Blueprint Arrow by rawpixel.com on [Freepik](https://www.freepik.com/free-vector/hand-drawn-arrow-illustration-collection_3854493.htm)
- Grey Bear Bite Attack remixed from [Bear Face](https://game-icons.net/1x1/sparker/bear-face.html) & [Snake Bite](https://game-icons.net/1x1/lorc/snake-bite.html)
- Grey Bear Slash Attack from [Bear Pawprint](https://www.svgrepo.com/svg/111459/bear-pawprint)
- Grey Bear Armor from [Bear](https://www.svgrepo.com/svg/441800/bear)
- Husky Bite Attack from [Wolf Head](https://www.svgrepo.com/svg/499455/wolf-head)
- Husky Armor remixed from [German Shepherd](https://www.svgrepo.com/svg/20680/german-shepherd)
- Pistol Silencer remixed from [Cylinder](https://www.svgrepo.com/svg/221203/cylinder)

## Sounds

- River Crossing Wind by [Astounded on Freesound](https://freesound.org/people/Astounded/sounds/483479/)
